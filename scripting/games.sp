#include <sourcemod>

new String:file_two[PLATFORM_MAX_PATH];

new	Handle:kv_two	=	INVALID_HANDLE;


public Plugin:myinfo =
{
	name = "Games Voting",
	description = "Голосование за игры среди Т",
	author = "Darkeneez",
	version = "1.0.2",
	url = "http://darkeneez.ucoz.net/",
};

public OnPluginStart()
{
	RegConsoleCmd("sm_games", games_);
	RegConsoleCmd("sm_game", games_);
	RegAdminCmd("sm_rgame", rgames);
	
	BuildPath(PathType:0, file_two, 256, "configs/games_voting/games_rules_menu.cfg");
	
	kv_two = CreateKeyValues("rules", "", "");
	if (!FileToKeyValues(kv_two, file_two))
	{
		SetFailState("File %s not founded", file_two);
	}
	return 0;
}


bool:StrEqual(String:str1[], String:str2[], bool:caseSensitive)
{
	return strcmp(str1, str2, caseSensitive) == 0;
}

PrintToChatAll(String:format[])
{
	decl String:buffer[192];
	new i = 1;
	while (i <= MaxClients)
	{
		if (IsClientInGame(i))
		{
			SetGlobalTransTarget(i);
			VFormat(buffer, 192, format, 2);
			PrintToChat(i, "%s", buffer);
		}
		i++;
	}
	return 0;
}

PrintCenterTextAll(String:format[])
{
	decl String:buffer[192];
	new i = 1;
	while (i <= MaxClients)
	{
		if (IsClientInGame(i))
		{
			SetGlobalTransTarget(i);
			VFormat(buffer, 192, format, 2);
			PrintCenterText(i, "%s", buffer);
		}
		i++;
	}
	return 0;
}

PrintHintTextToAll(String:format[])
{
	decl String:buffer[192];
	new i = 1;
	while (i <= MaxClients)
	{
		if (IsClientInGame(i))
		{
			SetGlobalTransTarget(i);
			VFormat(buffer, 192, format, 2);
			PrintHintText(i, "%s", buffer);
		}
		i++;
	}
	return 0;
}

public OnMapStart()
{
	GetCurrentMap(map, 256);
	kv_dym = CreateKeyValues("map", "", "");
	BuildPath(PathType:0, map, 256, "configs/games_voting/%s.cfg", map);
	if (FileToKeyValues(kv_dym, map))
	{
		g_imap = true;
	}
	return 0;
}

public Action:rgames(client, args)
{
	GetCurrentMap(map, 256);
	kv_dym = CreateKeyValues("map", "", "");
	BuildPath(PathType:0, map, 256, "configs/games_voting/%s.cfg", map);
	if (FileToKeyValues(kv_dym, map))
	{
		g_imap = true;
	}
	return Action:3;
}

public Action:games_(client, args)
{
	new var1;
	if (GetClientTeam(client) == 3 && !IsPlayerAlive(client))
	{
		return Action:3;
	}
	MainMenu(client);
	return Action:3;
}

MainMenu(client)
{
	new Handle:panel_ = CreatePanel(Handle:0);
	SetPanelTitle(panel_, "Servers-Info.Ru", false);
	DrawPanelItem(panel_, "Правила игр", 0);
	DrawPanelItem(panel_, "Голосования\n \n", 0);
	SetPanelCurrentKey(panel_, 10);
	DrawPanelItem(panel_, "Выход", 16);
	SendPanelToClient(panel_, client, PanelSelect_one, 0);
	return 0;
}

public PanelSelect_one(Handle:panel, MenuAction:action, client, option)
{
	if (action == MenuAction:4)
	{
		if (option == 1)
		{
			CreateInfoMenu(client);
		}
		if (option == 2)
		{
			vote_type(client);
		}
	}
	ClientCommand(client, "playgamesound items/nvg_off.wav");
	return 0;
}

CreateInfoMenu(client)
{
	new Handle:_menu = CreateMenu(Handle_rules, MenuAction:28);
	SetMenuTitle(_menu, "Servers-Info.Ru");
	if (KvGotoFirstSubKey(kv_two, false))
	{
		new String:name_[256];
		do {
			KvGetSectionName(kv_two, name_, 256);
			AddMenuItem(_menu, name_, name_, 0);
		} while (KvGotoNextKey(kv_two, false));
	}
	KvRewind(kv_two);
	SetMenuExitButton(_menu, true);
	DisplayMenu(_menu, client, 0);
	return 0;
}

public Handle_rules(Handle:menu, MenuAction:action, client, info)
{
	if (action == MenuAction:16)
	{
		CloseHandle(menu);
		return 0;
	}
	if (action != MenuAction:4)
	{
		return 0;
	}
	new String:name[76];
	GetMenuItem(menu, info, name, 75, 0, "", 0);
	ShowRulesPanel(client, name);
	return 0;
}

ShowRulesPanel(client, String:game[])
{
	if (KvJumpToKey(kv_two, game, false))
	{
		new Handle:panel_ = CreatePanel(Handle:0);
		new String:info[256];
		Format(info, 256, "|=====%s====|", game);
		SetPanelTitle(panel_, game, false);
		new p_min = KvGetNum(kv_two, "min_players", 0);
		new p_max = KvGetNum(kv_two, "max_players", 0);
		new String:rule[256];
		new String:example[256];
		KvGetString(kv_two, "rules", rule, 256, "");
		KvGetString(kv_two, "example", example, 256, "");
		Format(info, 256, "Минимум игроков: %d", p_min);
		DrawPanelText(panel_, info);
		Format(info, 256, "Максимум игроков: %d", p_max);
		DrawPanelText(panel_, info);
		Format(info, 256, "Правила: %s", rule);
		DrawPanelText(panel_, info);
		Format(info, 256, "Пример: %s", example);
		DrawPanelText(panel_, info);
		DrawPanelText(panel_, "\n \n");
		SetPanelCurrentKey(panel_, 8);
		DrawPanelItem(panel_, "Назад", 16);
		SetPanelCurrentKey(panel_, 10);
		DrawPanelItem(panel_, "Выход", 16);
		SendPanelToClient(panel_, client, PanelSelect, 0);
		KvRewind(kv_two);
		return 0;
	}
	return 0;
}

public PanelSelect(Handle:panel, MenuAction:action, client, option)
{
	if (action == MenuAction:4)
	{
		if (option == 8)
		{
			CreateInfoMenu(client);
		}
	}
	return 0;
}

create_vote(client, Handle:kv, bool:random)
{
	if (IsVoteInProgress(Handle:0))
	{
		PrintToChat(client, "\x04[GamesVote]\x01Голосование УЖЕ идет");
		return 0;
	}
	new i_Clients[32];
	new i_Count;
	new i = 1;
	while (i <= MaxClients)
	{
		new var1;
		if (!IsClientInGame(i) || !IsPlayerAlive(i) || GetClientTeam(i) == 2)
		{
		}
		else
		{
			i_Count++;
			i_Clients[i_Count] = i;
		}
		i++;
	}
	new Handle:_menu = CreateMenu(Handle_VoteMenu, MenuAction:28);
	SetMenuTitle(_menu, "Во что хотите поиграть?");
	if (KvGotoFirstSubKey(kv, false))
	{
		new String:name_[72];
		new p_max;
		new p_min;
		do {
			KvGetSectionName(kv, name_, 70);
			p_max = KvGetNum(kv, "max_players", 0);
			p_min = KvGetNum(kv, "min_players", 0);
			new var2;
			if (p_min <= i_Count && p_max >= i_Count)
			{
				if (!random)
				{
					AddMenuItem(_menu, name_, name_, 0);
				}
				new var3;
				if (GetRandomInt(1, 5) == 2 || 3)
				{
					AddMenuItem(_menu, name_, name_, 0);
				}
			}
		} while (KvGotoNextKey(kv, false));
	}
	KvRewind(kv);
	VoteMenu(_menu, i_Clients, i_Count, 10, 0);
	return 0;
}

vote_type(client)
{
	new Handle:_menu = CreateMenu(Handle_Map, MenuAction:28);
	SetMenuTitle(_menu, "Servers-Info.Ru");
	if (!g_imap)
	{
		AddMenuItem(_menu, "mgame", "Игры на карте", 1);
	}
	else
	{
		AddMenuItem(_menu, "mgame", "Игры на карте", 0);
	}
	AddMenuItem(_menu, "game", "Общие игры\n \n", 0);
	DisplayMenu(_menu, client, 0);
	return 0;
}

public Handle_Map(Handle:menu, MenuAction:action, client, info)
{
	if (action == MenuAction:16)
	{
		CloseHandle(menu);
		return 0;
	}
	if (action == MenuAction:4)
	{
		new String:menu_[12];
		GetMenuItem(menu, info, menu_, 10, 0, "", 0);
		if (StrEqual(menu_, "mgame", true))
		{
			create_vote(client, kv_dym, false);
		}
		else
		{
			create_vote(client, kv_two, true);
		}
	}
	return 0;
}

public Handle_VoteMenu(Handle:menu, MenuAction:action, param1, param2)
{
	if (0 < param1)
	{
		new String:game[64];
		GetMenuItem(menu, param2, game, 64, 0, "", 0);
		if (!StrEqual(game, "", true))
		{
			PrintToChatAll("\x04[GamesVote]\x03%N \x01Хочет играть в \x03\"%s\"", param1, game);
		}
	}
	if (action == MenuAction:16)
	{
		CloseHandle(menu);
	}
	else
	{
		if (action == MenuAction:32)
		{
			new String:game[64];
			GetMenuItem(menu, param1, game, 64, 0, "", 0);
			PrintToChatAll("\x04[GamesVote]\x01Играем в \x03\"%s\"", game);
			PrintCenterTextAll("Играем в %s", game);
			PrintHintTextToAll("Играем в %s", game);
			if (KvJumpToKey(kv_two, game, false))
			{
				new String:rules_[256];
				KvGetString(kv_two, "rules", rules_, 256, "");
				if (!StrEqual(rules_, "", true))
				{
					PrintToChatAll("\x04[GamesVote]\x03Правила игры в \x04%s:\x03\n%s", game, rules_);
				}
			}
			KvRewind(kv_two);
		}
	}
	return 0;
}

